# README #

This README documents steps that are necessary to get your application up and running.

### What is this repository for? ###

* Repository is for demo project done through self learning process of Protractor testing.

### How do I get set up? ###

* Clone project from git repository

* Open project and install necessary modules

* npm install protractor

* npm install chai

* npm install fastcsv

* npm install faker

* npm install --save-dev @types/chai @types/cucumber chai cucumber protractor-cucumber-framework

* Run regression test suite through terminal:
* protractor .\conf.js --cucumberOpts.tags="@regression"

* Run smoke test suite through terminal:
* protractor .\conf.js --cucumberOpts.tags="@smoke"

### Guidelines ###

* Folder feature - where the test scenarios are

* Folder stepDefinitions - where the methodes glued to scenario steps are

* Folder pages - where page object models with defined methods are

* Folder helper - where global helping methods that can be used in every page are

* conf.js - file where all important settings for project are

* Folder report - where cucumber report, csv file are generated

### Who do I talk to? ###

* Marko Vukovic
* Hope this quick readme file helps if not contact me to marko.vukovickg95@gmail.com