Feature: Practicing checkboxes

    @smoke
    Scenario Outline: Go to checkboxes website and chose them
        Given User navigates to url: "http://www.way2automation.com/angularjs-protractor/checkboxes/"
        Then User clear all checkboxes
        Then User chose home improvement checkbox "<home>"
        Then User chose painting checkbox "<painting>"
        Then User chose garage improvement checkbox "<garage>"
        Then User chose car checkbox "<car>"

        Examples:
            | home        | painting              | garage          | car           |
            | Screwdriver | Green Paint,Red Paint | Leaf Blower,Axe | First Aid Kit |