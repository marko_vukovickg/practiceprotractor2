Feature: Shopping for online course

    @smoke
    Scenario: Go to shopping webiste and shop for online course
        Given User navigates to url: "http://practice.automationtesting.in/"
        When User navigates to catalog
        Then User add product to basket
        Then User navigates to shopping basket and proceedes to checkout
        Then User should populates order form
            | firstName | lastName | email         | address            | phone      | country | company | town       | state  | postcode | payment |
            | Joe       | Gomez    | test@test.com | Narodnog Heroja 12 | 0611553311 | Male    | SBB     | Kragujevac | Serbia | 34000    | cash    |
        Then User should see message "Thank you. Your order has been received."