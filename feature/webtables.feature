Feature: Practicing webtables

    @smoke
    Scenario: Go to webtables website and add new user
        Given User navigates to url: "http://www.way2automation.com/angularjs-protractor/webtables/"
        Then User clicks on add new user
        Then User fills form for new user
            | firstName   | lastName | email         | phone     | username | pass | role  |
            | Screwdriver | Green    | test@test.com | 062882992 | test     | test | Admin |


    @smoke
    Scenario: Go to webtables website and edit user
        Given User navigates to url: "http://www.way2automation.com/angularjs-protractor/webtables/"
        Then User clicks on edit user
        Then User clears existing data
        Then User fills form for new user
            | firstName   | lastName | email         | phone     | username | pass | role  |
            | Screwdriver | Green    | test@test.com | 062882992 | test     | test | Admin |


    @smoke
    Scenario: Go to webtables website and delete user
        Given User navigates to url: "http://www.way2automation.com/angularjs-protractor/webtables/"
        Then User clicks on delete user


    @smoke
    Scenario Outline: Save coronavirus data to csv
        Given User navigates to url: "https://www.worldometers.info/coronavirus/"
        Then User finds results for "<country>"
        Then User saves data on csv

        Examples:
            | country |
            | Serbia  |
        