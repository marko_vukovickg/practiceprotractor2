Feature: Practicing automation on winwin website

    @smoke
    Scenario Outline: Add product and check basket from win win website
        Given User navigates to url: "https://www.winwin.rs/"
        Then User search for "<product>"
        Then User sorts product by price "<sort>"
        Then User choses product type "<type>" and rating "<rating>"
        Then User checks if status is "Na stanju"
        Then User add selected product to basket
        Then User check for confirm message "uspešno dodat"

        Examples:
            | product  | sort          | type   | rating |
            | Kosilica | Cena rastuće | Benzin | 5      |


    @test
    Scenario: Apply for newsletter and check if successful
        Given User navigates to url: "https://www.winwin.rs/"
        Then User applies for newsletter "testTest@test.com"
        Then User check for confirm message "Zahtev za potvrdu je poslat."


    @smoke
    Scenario: Reggister account on WinWin
        Given User navigates to url: "https://www.winwin.rs/"
        Then User access registration form
        Then User fills the registration form
            | firstName | lastName | email         | newsletter | pass      | captcha |
            | Mister    | Johnes   | test@test.com | true       | Password1 | test    |
        Then User should see error message "Neispravna CAPTCHA."