let helper = require('../helper/helper.js');

var util = function () {

    this.utilElementClick = function (element) {
        helper.waitForElementPresent(element);
        helper.waitForElementDisplayed(element);
        element.click();
    }

    this.utilElementSendText = function (element, text) {
        helper.waitForElementPresent(element);
        helper.waitForElementDisplayed(element);
        element.sendKeys(text);
    }

    this.utilJavaScriptElementClick = function (element) {
        helper.waitForElementPresent(element);
        helper.waitForElementDisplayed(element);
        browser.executeScript('arguments[0].click();', element.getWebElement()); 
    }
}


module.exports = new util();