let helper = require('../helper/helper.js');
let util = require('../helper/util.js');

let shoppingPOM = function () {

    let store = element(by.xpath("//h2[contains(text(),'Store')]//input"));

    //Double click to get all checkboxes clear
    this.clearCheckBoxes = function (home) {
        util.utilElementClick(store);
        util.utilElementClick(store);
    }

    this.choseCheckboxOption = function (chosenOption) {
        if (chosenOption.includes(',')) {

            let partsList = chosenOption.split(',');

            for (var i = 0; i < partsList.length; i++) {
                element(by.xpath("//h4[contains(text(),'" + partsList[i] + "')]//input")).click();
            }

        } else {
            element(by.xpath("//h4[contains(text(),'" + chosenOption + "')]//input")).click();
        }
    }

};

module.exports = new shoppingPOM();