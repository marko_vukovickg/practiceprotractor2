let helper = require('../helper/helper.js');
let util = require('../helper/util.js');

let shoppingPOM = function () {

    let shopCatalog = element(by.id('menu-item-40'))
    let catalogItems = element.all(by.xpath("//a[contains(text(),'Add to basket')]"));
    let viewBasketBtn = element.all(by.xpath("//a[contains(text(),'View Basket')]"));
    let checkOutBtn = element.all(by.xpath("//a[contains(text(),'Proceed to Checkout')]"));

    let shoppingCart = element(by.id('wpmenucartli'))

    let firstNameInput = element(by.id('billing_first_name'))
    let firstLastInput = element(by.id('billing_last_name'))
    let companyInput = element(by.id('billing_company'))
    let emailInput = element(by.id('billing_email'))
    let phoneInput = element(by.id('billing_phone'))
    let addressInput = element(by.id('billing_address_1'))
    let cityInput = element(by.id('billing_city'))
    let stateInput = element(by.id('billing_state'))
    let postcodeInput = element(by.id('billing_postcode'))
    let paymentMethodBtn = element(by.id('payment_method_cod'))
    let placeOrderBtn = element(by.id('place_order'))

    // let checkOutBtn = element.all(by.xpath("//p[contains(text(),'Thank you. Your order has been received.')]"));
    let confirmationMsg = $$('.woocommerce-thankyou-order-received');


    this.getURL = function (url) {
        browser.driver.get(url);
    }

    this.navigateToCatalog = function () {
        util.utilElementClick(shopCatalog);
    }

    this.addProductToBasket = async function () {
        await util.utilElementClick(catalogItems.get(0));
    }

    this.navigateToCart = async function () {
        await util.utilElementClick(shoppingCart);
    }

    this.performCheckout = async function () {
        await util.utilElementClick(checkOutBtn);
        await browser.sleep(6000);
    }

    this.enterBillingData = async function (firstName, lastName, email, address, phone, country, company, town, state, postcode, payment) {
        await util.utilElementSendText(addressInput, address);
        await util.utilElementSendText(firstNameInput, firstName);
        await util.utilElementSendText(firstLastInput, lastName);
        await util.utilElementSendText(companyInput, company);
        await util.utilElementSendText(emailInput, email);
        await util.utilElementSendText(phoneInput, phone);
        await util.utilElementSendText(cityInput, town);
        await util.utilElementSendText(stateInput, state);
        await util.utilElementSendText(postcodeInput, postcode);
        await util.utilElementClick(paymentMethodBtn);
        await util.utilElementClick(placeOrderBtn);
    }

    this.checkIfConfirmMsgPresent = function () {
        helper.waitForElementPresent(confirmationMsg);
        helper.waitForElementDisplayed(confirmationMsg);
        return confirmationMsg.getText();
    }
};

module.exports = new shoppingPOM();