let helper = require('../helper/helper.js');
let util = require('../helper/util.js');

var fs = require('fs');
var csv = require('fast-csv');
var ws = fs.createWriteStream('./report/my.csv');

let webtablesPOM = function () {

    let addUserBtn = element(by.xpath("//button[contains(text(),'User')]"));
    let firstNameInput = element(by.xpath("//input[@name='FirstName']"));
    let lastNameInput = element(by.xpath("//input[@name='LastName']"));
    let userNameInput = element(by.xpath("//input[@name='UserName']"));
    let passwordInput = element(by.xpath("//input[@name='Password']"));
    let roleList = element.all(by.xpath("//select[@name='RoleId']//*"));
    let emailInput = element(by.xpath("//input[@name='Email']"));
    let mobilePhoneInput = element(by.xpath("//input[@name='Mobilephone']"));
    let customer = element.all(by.xpath("//input[@type='radio']"));
    let saveBtn = element.all(by.xpath("//button[contains(text(),'Save')]"));

    let editButton = element.all(by.xpath("//button[contains(text(),'Edit')]"));
    let removeButton = element.all(by.xpath("//i[@class='icon icon-remove']"));
    let confirmDeleteButton = element(by.xpath("//button[contains(text(),'OK')]"));

    let deleteDialog = element(by.xpath("//div[@class='modal-footer']"));

    let tableHeaderData = element.all(by.xpath("//table[@id='main_table_countries_today']//thead//tr//th"));

    this.choseAddUser = function () {
        util.utilElementClick(addUserBtn);
    }

    this.enterNewUserData = async function (firstName, lastName, email, phone, username, pass, role) {
        await util.utilElementSendText(firstNameInput, firstName);
        await util.utilElementSendText(lastNameInput, lastName);
        await util.utilElementSendText(emailInput, email);
        await util.utilElementSendText(mobilePhoneInput, phone);
        await util.utilElementSendText(userNameInput, username);
        await util.utilElementSendText(passwordInput, pass);
        await util.utilElementClick(customer.get(1));
        this.choseRole(role);
        await util.utilElementClick(saveBtn);
    }

    this.choseRole = async function (role) {
        var i = 0;
        if (role == "Sales Team") {
            i = 1;
        } else if (role == "Customer") {
            i = 2;
        } else if (role == "Admin") {
            i = 3;
        }
        await util.utilElementClick(roleList.get(i));
    }

    this.clickEditUser = function () {

        util.utilElementClick(editButton);
    }

    this.clearExistingData = async function () {
        firstNameInput.clear();
        lastNameInput.clear();
        emailInput.clear();
        mobilePhoneInput.clear();
        userNameInput.clear();
        passwordInput.clear();
    }

    this.openDeleteDataDialog = async function () {
        await util.utilElementClick(removeButton);
        browser.switchTo().activeElement();
    }


    this.confirmDeleteData = async function () {
        await util.utilElementClick(confirmDeleteButton);
    }


    this.findResultsForCountry = function (country) {
        let selectedCountry = element(by.xpath("//tr//td//a[contains(text(),'" + country + "')]"));
        util.utilJavaScriptElementClick(selectedCountry);
    }



    this.saveToCsv = async function () {

        let numberedData = element.all(by.xpath("//div[@class='maincounter-number']//span"));

        let sumCases;
        let deaths;
        let recovered;

        await numberedData.get(0).getText().then(function (actual) {
            sumCases = actual;
        });

        await numberedData.get(1).getText().then(function (actual) {
            deaths = actual;
        });

        let recoveredObject = element.all(by.xpath("//div[@class='maincounter-number' and @style]//span")).getText();

        recoveredObject.then(elementText => { console.log("INFORMACIJE" + elementText); });

        csv.write(
            [
                ["Coronavirus Cases:", "Deaths", "Recovered"],
                [sumCases, deaths, recovered]
            ], { headers: true }).pipe(ws);

    }

};

module.exports = new webtablesPOM();