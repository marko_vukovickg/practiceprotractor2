let helper = require('../helper/helper.js');
let util = require('../helper/util.js');

let winwinPOM = function () {
    let searchInput = element(by.id("search"));
    let searchButton = element(by.xpath("//button[@title='Pretraga']"));
    let statusField = element(by.xpath("//span[contains(text(),'stanju')]"));
    let addToCart = element(by.id("product-addtocart-button"));
    let succesMsg = element(by.xpath("//li[@class='success-msg']//ul//li//span"));
    let noteMsg = element(by.xpath("//p[contains(text(),'Vaša pretraga nije dala rezultate.')]"));
    let newsletterInput = element(by.id("newsletter"));
    let applyNewsletterButton = element(by.xpath("//button[@title='Prijavite se']"));
    let registrationFormAccess = element(by.xpath("//a[contains(text(),'Nemate nalog')]"));
    let errorMsg = element(by.xpath("//li[@class='error-msg']//ul//li//span"));

    let firstNameInput = element(by.id("firstname"));
    let lastNameInput = element(by.id("lastname"));
    let passInput = element(by.id("password"));
    let passConfirmInput = element(by.id("confirmation"));
    let captchaInput = element(by.id("captcha_user_create"));
    let newsletterCheckbox = element(by.id("is_subscribed"));
    let emailInput = element(by.id("email_address"));
    let confirmRegisterBtn = element(by.xpath("//button[@title='Pošalji']"));

    this.searchForProduct = async function (product) {
        try {
            util.utilElementSendText(searchInput, product);
            helper.waitForElementDisplayed(searchButton);
            helper.waitForElementPresent(searchButton);
            util.utilElementClick(searchButton);
            await browser.sleep(2000);

        } catch (err) {
            console.log('Error has occurred: ' + err.stack);
        }
    }

    this.navigateDirectlylawnMower = async function () {
        if (this.checkSearch() == true) {
            browser.get('https://www.winwin.rs/alati-i-basta/sve-za-bastu/bastovanstvo/kosilice-za-travu.html');
            await browser.sleep(2000);
            browser.getCurrentUrl().then(function (actual) {
                expect(actual).to.include("kosilice-za-travu", 'Expected is:' + "kosilice-za-travu" + ' Actual is:' + actual);
            });
        }
    }


    this.checkSearch = function () {
        try {
            helper.waitForElementDisplayed(noteMsg);
            helper.waitForElementPresent(noteMsg);
            util.utilElementClick(noteMsg)
            return true;
        } catch (err) {
            console.log('There is no note msg: ' + err);
            return false;
        }
    }


    this.applyForNewsletter = function (email) {
        try {
            helper.waitForElementDisplayed(newsletterInput);
            helper.waitForElementPresent(newsletterInput);
            util.utilElementSendText(newsletterInput, email)

            helper.waitForElementDisplayed(applyNewsletterButton);
            helper.waitForElementPresent(applyNewsletterButton);
            util.utilJavaScriptElementClick(applyNewsletterButton);
        } catch (err) {
            console.log('There is no newsletter box: ' + err);
        }
    }


    this.sortProductResult = async function (sort) {
        let sortOption = element(by.xpath("//option[contains(text(),'" + sort + "')]"));
        helper.waitForElementDisplayed(sortOption);
        util.utilElementClick(sortOption);
        util.utilJavaScriptElementClick(sortOption);
        await browser.sleep(2000);
        browser.getCurrentUrl().then(function (actual) {
            expect(actual).to.include("dir=asc&order=price", 'Expected is:' + "dir=asc&order=price" + ' Actual is:' + actual);
        });
    }


    this.choseProductWithin = async function (type, rating) {
        let chosenProduct = element(by.xpath("//span[contains(text(),'" + type + "')]//parent::a//parent::h2//following-sibling::div//span[@itemprop='ratingValue' and contains(text(),'" + rating + "')]"));
        helper.waitForElementDisplayed(chosenProduct);
        util.utilJavaScriptElementClick(chosenProduct);
        await browser.sleep(2000);
        browser.getCurrentUrl().then(function (actual) {
            expect(actual).to.include("benzinska", 'Expected is:' + "benzinska" + ' Actual is:' + actual);
        });
    }


    this.checkStatusOfProduct = async function () {
        helper.waitForElementDisplayed(statusField);
        helper.waitForElementPresent(statusField);
        return statusField.getText();
    }

    this.addProductToCart = function () {
        util.utilJavaScriptElementClick(addToCart);
    }

    this.checkForConfirmMsg = function () {
        helper.waitForElementDisplayed(succesMsg);
        helper.waitForElementPresent(succesMsg);
        return succesMsg.getText();
    }

    this.accessRegistrationForm = function () {
        helper.waitForElementDisplayed(registrationFormAccess);
        helper.waitForElementPresent(registrationFormAccess);
        util.utilJavaScriptElementClick(registrationFormAccess);
    }

    this.checkForErrorMsg = function () {
        helper.waitForElementDisplayed(errorMsg);
        helper.waitForElementPresent(errorMsg);
        return errorMsg.getText();
    }


    this.enterRegistrationData = async function (firstName, lastName, email, newsletter, captcha, pass) {
        await util.utilElementSendText(emailInput, email);
        await util.utilElementSendText(firstNameInput, firstName);
        await util.utilElementSendText(lastNameInput, lastName);
        await util.utilElementSendText(passInput, pass);
        await util.utilElementSendText(passConfirmInput, pass);
        await util.utilElementSendText(captchaInput, captcha);

        if (newsletter == "true") {
            await util.utilElementClick(newsletterCheckbox);
        }

    }


    this.confirmRegistration = async function () {
        helper.waitForElementDisplayed(confirmRegisterBtn);
        helper.waitForElementPresent(confirmRegisterBtn);
        await util.utilElementClick(confirmRegisterBtn);
    }
};

module.exports = new winwinPOM();