var { Given, Then, When } = require('cucumber');
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
let checkboxPOM = require('../pages/checkboxPOM.js');


Then('User clear all checkboxes', async function () {
    checkboxPOM.clearCheckBoxes();
    await browser.sleep(3000);
});


Then('User chose home improvement checkbox {string}', async function (home) {
    checkboxPOM.choseCheckboxOption(home);
    await browser.sleep(3000);
});


Then('User chose painting checkbox {string}', async function (painting) {
    checkboxPOM.choseCheckboxOption(painting);
    await browser.sleep(3000);
});


Then('User chose garage improvement checkbox {string}', async function (garage) {
    checkboxPOM.choseCheckboxOption(garage);
    await browser.sleep(3000);
});


Then('User chose car checkbox {string}', async function (car) {
    checkboxPOM.choseCheckboxOption(car);
    await browser.sleep(3000);
});