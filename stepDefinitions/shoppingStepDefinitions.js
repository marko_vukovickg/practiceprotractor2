var { Given, Then, When } = require('cucumber');
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
let shoppingPOM = require('../pages/shoppingPOM.js');
var _ = require('lodash');
var Q = require('q');


Given('User navigates to url: {string}', async function (url) {
    try {
        await browser.get(url);
    } catch (err) {
        console.log('Error has occurred: ' + err.stack);
    }
});


When('User navigates to catalog', async function () {
    shoppingPOM.navigateToCatalog();
    await browser.sleep(3000);
});


When('User add product to basket', async function () {
    shoppingPOM.addProductToBasket();
    await browser.sleep(4000);
});


Then('User navigates to shopping basket and proceedes to checkout', async function () {
    shoppingPOM.navigateToCart();
    await browser.sleep(5000);
    shoppingPOM.performCheckout();
    await browser.sleep(5000);

});

Then('User should populates order form', async function (data) {
    await browser.sleep(4000);

    
    var promises = [];
    var rows = data.hashes();
    //For each row you will get the user and the country
    _.each(rows, function (row) {
        var firstName = row.firstName;
        var lastName = row.lastName;
        var email = row.email;
        var address = row.address;
        var phone = row.phone;
        var country = row.country;
        var company = row.company;
        var town = row.town;
        var state = row.state;
        var postcode = row.postcode;
        var payment = row.payment;

        //Here you can add the promises to perform sequentially
        //you can call the promises passing the user and the 
        //country as parameters
        promises.push((shoppingPOM.enterBillingData(firstName, lastName, email, address, phone, country, company, town, state, postcode, payment)));

    });
    await browser.sleep(4000);
    // return Q.all(promises);
});


Then('User should see message {string}', async function (confirmMsg) {// 
    let msg = shoppingPOM.checkIfConfirmMsgPresent();
    await msg.then(function (actual) {
        console.log('OVDE' + msg);
        expect(actual).to.equal(confirmMsg, 'Expected is:' + confirmMsg + ' Actual is:' + actual);
    });
    await browser.sleep(2000);
});