var { Given, Then, When } = require('cucumber');
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
let webtablesPOM = require('../pages/webtablesPOM.js');
var _ = require('lodash');
var Q = require('q');


Then('User clicks on add new user', async function () {
    webtablesPOM.choseAddUser();
    await browser.sleep(3000);
});


Then('User clicks on edit user', async function () {
    webtablesPOM.clickEditUser();
    await browser.sleep(3000);
});


Then('User clears existing data', async function () {
    webtablesPOM.clearExistingData();
    await browser.sleep(3000);
});


Then('User clicks on delete user', async function () {
    webtablesPOM.openDeleteDataDialog();
    await browser.sleep(2000);
    webtablesPOM.confirmDeleteData();
    await browser.sleep(3000);
    webtablesPOM.saveToCsv();
});


Then('User saves data on csv', async function () {
    await browser.sleep(2000);
    webtablesPOM.saveToCsv();
});


Then('User finds results for {string}', async function (country) {
    webtablesPOM.findResultsForCountry(country);
    await browser.sleep(6000);

});


Then('User fills form for new user', async function (data) {
    await browser.sleep(4000);

    var promises = [];
    var rows = data.hashes();
    //For each row you will get the user and the country
    _.each(rows, function (row) {
        var firstName = row.firstName;
        var lastName = row.lastName;
        var email = row.email;
        var phone = row.phone;
        var username = row.username;
        var pass = row.pass;
        var role = row.role;

        
        //Here you can add the promises to perform sequentially
        //you can call the promises passing the user and the 
        //country as parameters
        promises.push((webtablesPOM.enterNewUserData(firstName, lastName, email, phone, username, pass, role)));
    });
    await browser.sleep(4000);
    // return Q.all(promises);
});