var { Given, Then, When } = require('cucumber');
var { setDefaultTimeout } = require('cucumber');
setDefaultTimeout(60 * 1000);
let winwinPOM = require('../pages/winwinPOM.js');
var _ = require('lodash');
var Q = require('q');

Then('User search for {string}', async function (product) {
    winwinPOM.searchForProduct(product);
    await browser.sleep(3000);
    if (product == 'Kosilica') {
        winwinPOM.navigateDirectlylawnMower();
        await browser.sleep(2000);
    }
});


Then('User sorts product by price {string}', async function (sort) {
    await browser.sleep(3000);
    winwinPOM.sortProductResult(sort);
    await browser.sleep(3000);
});


Then('User choses product type {string} and rating {string}', async function (type, rating) {
    winwinPOM.choseProductWithin(type, rating);
    await browser.sleep(3000);
});

Then('User checks if status is {string}', async function (status) {
    await browser.sleep(3000);
    await winwinPOM.checkStatusOfProduct().then(function (actual) {
        expect(actual).to.equal(status, 'Expected is:' + status + ' Actual is:' + actual);
    });
    await browser.sleep(3000);
});


Then('User add selected product to basket', async function () {
    await browser.sleep(3000);
    winwinPOM.addProductToCart();
    await browser.sleep(3000);
});


Then('User check for confirm message {string}', async function (msg) {
    await browser.sleep(3000);
    await winwinPOM.checkForConfirmMsg().then(function (actual) {
        expect(actual).to.include(msg, 'Expected is:' + msg + ' Actual is:' + actual);
    });

    await browser.sleep(3000);
});


Then('User applies for newsletter {string}', async function (email) {
    await browser.sleep(2000);
    await winwinPOM.applyForNewsletter(email);
    await browser.sleep(2000);
});


Then('User access registration form', async function () {
    await browser.sleep(2000);
    await winwinPOM.accessRegistrationForm();
    await browser.sleep(2000);
});


Then('User fills the registration form', async function (data) {
    await browser.sleep(4000);

    var promises = [];
    var rows = data.hashes();
    //For each row you will get the user and the country
    _.each(rows, function (row) {
        var firstName = row.firstName;
        var lastName = row.lastName;
        var email = row.email;
        var newsletter = row.newsletter;
        var pass = row.pass;
        var captcha = row.captcha;

        
        //Here you can add the promises to perform sequentially
        //you can call the promises passing the user and the 
        //country as parameters
        promises.push((winwinPOM.enterRegistrationData(firstName, lastName, email, newsletter, captcha, pass)));
    });
    await browser.sleep(3000);
    winwinPOM.confirmRegistration();
    // return Q.all(promises);
});


Then('User should see error message {string}', async function (errMsg) {
    await browser.sleep(3000);
    await winwinPOM.checkForErrorMsg().then(function (actual) {
        expect(actual).to.include(errMsg, 'Expected is:' + errMsg + ' Actual is:' + actual);
    });

    await browser.sleep(3000);
});
